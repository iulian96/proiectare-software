package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;


public interface UserService {
    public void saveUser(User user);
    public boolean isUserAlreadyPresent(User user);
}
