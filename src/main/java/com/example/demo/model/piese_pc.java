package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class piese_pc {
    @Id
    @GeneratedValue
    @Column(name = "idpiesa")
    private Integer idpiesa;
    @Column(name = "denumire")
    private String denumire;
    @Column(name = "cod_garantie")
    private String cod_garantie;
    @Column(name = "pret")
    private Integer pret;
    @Column(name = "cantitate")
    private Integer cantitate;
    @Column(name = "producator")
    private String producator;

    public piese_pc() {
    }

    public Integer getIdpiesa() {
        return idpiesa;
    }

    public void setIdpiesa(Integer idpiesa) {
        this.idpiesa = idpiesa;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public String getCod_garantie() {
        return cod_garantie;
    }

    public void setCod_garantie(String cod_garantie) {
        this.cod_garantie = cod_garantie;
    }

    public Integer getPret() {
        return pret;
    }

    public void setPret(Integer pret) {
        this.pret = pret;
    }

    public Integer getCantitate() {
        return cantitate;
    }

    public void setCantitate(Integer cantitate) {
        this.cantitate = cantitate;
    }

    public String getProducator() {
        return producator;
    }

    public void setProducator(String producator) {
        this.producator = producator;
    }
}
