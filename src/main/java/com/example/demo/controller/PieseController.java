package com.example.demo.controller;

import com.example.demo.model.piese_pc;
import com.example.demo.repository.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/alldata")
public class PieseController {

    @Autowired
    DataRepository dataRepository;

    @GetMapping("all")
    public List<piese_pc> getAll(){
    return dataRepository.findAll();
    }
    @PostMapping("load")
    public List<piese_pc> persist(@RequestBody final piese_pc piese_pc){
        dataRepository.save(piese_pc);
        return dataRepository.findAll();
    }

}
