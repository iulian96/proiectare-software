package com.example.demo.repository;

import com.example.demo.model.piese_pc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DataRepository extends JpaRepository<piese_pc, Integer> {
}
