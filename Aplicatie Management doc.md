#Warehouse management system

Nume: Big Iulian Mihai Alin
Grupa: 30239

#Introducere
Am ales acest tip de proiect care are ca obiectiv proiectarea si implementarea unei aplicatii pentru imbunatatirea gestiunii stocurilor și a managementului depozitelor.
Consider ca un sistem de tip warehouse management este o componenta esentiala a lantului de aprovizionare, care are rolul de a monitoriza si controla miscarea stocurilor de marfa în cadrul depozitelor.
Practic, rolul unei solutii WMS de calitate este de a automatiza, optimiza și eficientiza activitățile din depozit ce tin de receptia, depozitarea, colectarea si expedierea marfurilor, oferind în același timp informații în timp real cu privire la stocuri (cantitati, date de expirare etc.). Totodata, cu ajutorul unui WMS integrat se poate reduce semnificativ durata procesului de inventariere sau reaprovizionare.

Pentru implementarea acestui tip de proiect am ales sa folosesc mediul de proiectare numit "Intellij", impreuna cu API-urile oferite de framework-ul SpringBoot.
Deocamdata, avand in vedere ca sunt la inceputul proiectului am implementat o pagina de log-in securizata cu BCrypt care are in spate o baza de date SQL si care sa permita accesul utilizatorilor (Admin si Angajat) in cadrul sistemului, de asemenea am implementat si o pagina de inregistrare pentru angajatii noi. 
->Pentru utilizatorul de tip angajat, dupa ce foloseste metoda de login poate sa vizualizeze obiectele din tabela "piese_pc" si nimic mai mult, urmeaza sa fie adaugate metode de tip POST, EDIT si DELETE care sa ii permita acestuia sa adauge, sa modifice si sa stearga obiectele din aceasta tabela.
->Utilizatorul de tip administrator o sa poata sa vada toate obiectele din tabelele care contin date legate de marfa din depozit, iar in plus o sa poata sa stearga sau sa adauge angajati noi. In acest moment aplicatia redirectioneaza catre o pagina de tip html simpla (doar pentru test).

In continuare o sa prezint pasii pe care i-am urmat pentru a implementa pagina de log-in si cea de inregistrare:
1. Am creeat tabelele pentru utilizatori care stocheaza id-ul, numele, emailul, tipul de utilizator si parola.
2. Am definit modele pentru tabelele din baza de date.
3. Am adaugat Bcrypt password encoder pentru securitatea din spatele panoului de log-in.
4. Am definit controllerele care afiseaza paginile de login respectiv inregistare.
5. Am adaugat codul HTML pentru aceste doua pagini si redirectionarile pentru accesarea lor din browser.




